# Changelogs

You can find a complete list of changelogs between versions



## Version 1.8.2

### New features

- Added support for Wikipedia italian for the metadata scraper

### Improvements

- Upgraded from Laravel 8 to Laravel 10
- Migrated from Laravel Mix to Laravel Vite

### Bug fixes

- Metadata card was not visible with the new immersive video player
- Wikipedia scraper no longer worked and has been restored



## Version 1.8.1

### Improvements

- Minor improvement to the immersive video player



## Version 1.8

### New features

- Immersive & full screen video player

### Improvements

- Upgraded to PHP 8.1



## Version 1.7

### New features

- Added resume from where you left feature for videos and audios
- Added file metadata feature. Files can now contain a poster, links, actors and many more data. You can also provide a link to a Wikipedia (english) page and MSL will automatically parse all available metadata.
- Users can now change their profile picture and name
- Users can now add, change and remove their own password
- Files that contains captions are now marked with a CC icon on the folder view
- History can be sorted by last opened or most opened
- Pages are now responsive and can adapt to smaller displays
- Better UI for file viewer that bundles breadcrumb and buttons in header

### Improvements

- JWT tokens are now automatically refreshed while using the application
- JWT tokens are now encrypted when saved on the browser
- Added SonarQube/SonarCloud for code quality and code security
- New features have more than 45 E2E tests

### Bug fixes

- Creating a user with no password created an invalid password, thus locking the user out of his account
- Path on Windows had some problems
- Removing a file or a folder from a shared folder no longer crashes the application
- Path traversal security bug for profile picture and poster uploads