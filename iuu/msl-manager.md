# Media Server Lite Manager

![MSL Manager Logo <](../assets/images/logo-manager-small.png)

Media Server Lite Manager (MSL Manager from now on) is a lightweight utility to download, install, update, configure and manage your Media Server Lite in an automated and user-friendly fashion.

Please note that MSL Manager is only available for Windows 10 and newer. You can try to run it on other Windows versions but it might not work correctly or not at all.



## Prerequisites

- An active internet connection
- Administrator privileges
- All other requisites listed [here](https://msl.francescosorge.com/#/download/windows)



## Get MSL Manager

Follow the instructions available on the [official website](https://msl.francescosorge.com/#/download/windows)



## Launch

Double click the "Media Server Lite Manager.exe" file and you will be greeted by this simple welcome screen.

![Main window <](../assets/images/manager/main.png)

The buttons provide access to different features of the application:

**Install/Update:** automatically installs or updates a Media Server Lite installation and configures dependencies to work at their best.

**Manage:** is used to launch or stop the Media Server Lite server. It is also used to add or remove shared folders to Media Server Lite. Lastly, it provides information about where Media Server Lite is installed, where does it stores your data and on which URL it is available.

**Uninstall:** removes a Media Server Lite installation. You are also asked if you want to keep personal data (like shared folders, avatars and the configurations you made) or to remove them entirely.

&nbsp;

Choose what you need to do and click the corresponding button.

------



## Install/Update

### Version selection

![Select release ><](../assets/images/manager/select-release.png)

Select a version (mostly like the latest stable) and click the "Next" button.



### Installation directory selection

![Select release ><](../assets/images/manager/directory-selection.png)

| Are you doing a... | Which directory to choose                                    |
| -----------------: | :----------------------------------------------------------- |
|      Fresh install | Choose a new and empty directory                             |
|             Update | Choose the same directory where you already installed Media Server Lite. All your data will persist in the process update. Do a backup if you don't trust the update process. |

After you're done selecting a directory, click the "Next" button.



### Dependencies

![Dependencies pre-download ><](../assets/images/manager/dependency-install-1.png)

Media Server Lite will automatically download, install and configure the core dependencies that it needs to run. Just click the "Download" button and wait for the process to finish. It might take several minutes.

![Dependencies download, install and configuration ><](../assets/images/manager/dependency-install-2.png)

The window should look like this while MSL Manager works. You might see an administrator privilege request at some point. This is needed by Apache in order to install the Windows service.

![Dependencies tasks finished ><](../assets/images/manager/dependency-install-3.png)

Once MSL Manager finishes all tasks related to core dependencies, you can click the "Next" button to go to the next stage.



### Application

![MSL Download and configuration ><](../assets/images/manager/msl-download-1.png)

MSL Manager will now download and install Media Server Lite. It might take several minutes.



### Installation completed

![All done window ><](../assets/images/manager/msl-done.png)

If everything goes fine, you should see this window.



You can now click the "Go to Management >" button. 

To read more about how to use the management window and configure Media Server Lite, go to the [Configure > MSL Manager (Windows)](configure/msl-manager.md) page.



## Uninstall

Open the Manager and click the "Uninstall" button on the welcome screen.

It will ask you to locate the Media Server Lite folder. Navigate your PC, select the folder and confirm.

![Are you sure you want to uninstall Media Server Lite dialog ><](../assets/images/manager/uninstall-1.png)

If you really want to uninstall Media Server Lite, click "Yes". Otherwise, click "No".

If you clicked "Yes", the next dialog will ask you if you want to also delete personal data and you have to either click:

![Personal data dialog <](../assets/images/manager/uninstall-2.png)

**Yes**: this will uninstall Media Server Lite and also delete your personal data

**No**: this will uninstall Media Server Lite while keeping personal data for future installations

**Cancel**: this will cancel the uninstall process of Media Server Lite, keeping both the application and your personal data

&nbsp;

At this point, MSL Manager will uninstall Media Server Lite and remove your personal data if you decided so.