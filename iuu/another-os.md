# Another operating system

Media Server Lite officially supports Windows, macOS and Linux operating systems.



If you're using something different, you may still be able to run Media Server Lite.



## Under the hood

Media Server Lite is powered under the hood by Apache, PHP and SQLite so any OS capable of running/interpreting those dependencies can run Media Server Lite (maybe with some bugs...).



## Options available

### Docker

Docker supports many operating systems besides Windows, macOS and Linux.

Check if your operating system works with Docker and then follow the guide for Linux.



### Standalone

Apache, PHP and SQLite are almost certainly available for your operating system, just download, install and configure them to work with Media Server Lite.

You can grab the Media Server Lite source code on [GitLab](https://gitlab.com/media-server-lite/media-server-lite).