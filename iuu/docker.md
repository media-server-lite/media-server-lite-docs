# Windows, macOS, Linux (Docker)

![Docker Logo <](../assets/images/logo-docker-small.png)

Media Server Lite can be installed on any operating system that supports [Docker](https://www.docker.com/).



## Prerequisites

- An active internet connection
- Docker already installed and configured on your computer
- All other requisites listed for your operating system:
  - [Windows](https://msl.francescosorge.com/#/download/windows)
  - [macOS](https://msl.francescosorge.com/#/download/macos)
  - [Linux](https://msl.francescosorge.com/#/download/linux)
- (Linux only): administrator privileges, [an user belonging to the Docker group](https://docs.docker.com/engine/install/linux-postinstall/) or [rootless mode](https://docs.docker.com/engine/security/rootless/)



## Installation & basic setup

For a basic installation with Docker, have a look at the respective download page for your operating system:

- [Windows](https://msl.francescosorge.com/#/download/windows)
- [macOS](https://msl.francescosorge.com/#/download/macos)
- [Linux](https://msl.francescosorge.com/#/download/linux)

We strongly suggest you having a look also at the [Intermediate setup chapter](#intermediate-setup).



## Update

1. Bring down the container
2. Download the latest version with `docker pull fsorge/msl`
3. Start the container again



## Intermediate setup

Since Media Server Lite is distributed as a Docker image through the Docker Hub, you can browse the image page containing some instructions for a more complex configuration.

These instructions applies to all operating systems.

Go to the [Configure > Docker page](/configure/docker).



## Docker Hub page

[Docker Hub Media Server Lite page](https://hub.docker.com/r/fsorge/msl).