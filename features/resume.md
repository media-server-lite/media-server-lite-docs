# Resume

With the resume feature, Media Server Lite will automatically keep track of your progress while watching a video and if you happen to stop watching it, Media Server Lite will automatically prompt you to resume from where you left the next time you'll open the video.



It will show a window like this one, where you can choose to resume or start from the beginning.

![Resume window ><](../assets/images/resume/resume-window.png)

Media Server Lite automatically removes the resume when you reach the last 10% of the length of the video.