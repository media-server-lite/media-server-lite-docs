# History

History keeps track of the latest files you open, so you can easily access them and resume from where you left.



## Adding an item to history

Simply opening a file (be it an image, a video, a playlist, etc.) adds the file to your history.



## Where to find the history

You can find your favorite folders and files in your home page.

![Home page with history](../assets/images/history/home.png)



## Clear history

You can clear your file history at any time, just go to the home page and click the **❌ Clear history** button

![Clear history button](../assets/images/history/clear-history.png)