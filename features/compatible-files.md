# Compatible files

Media Server Lite is compatible with many file formats and more are planned to be added in the coming releases.

## Videos

- MP4



## Images

- PNG
- JPG/JPEG
- GIF
- BMP



## Audios

- MP3



## Playlists

- M3U
- M3U8



**Note:** although these formats should be supported, the actual support depends on the client browser. Media Server Lite can stream any file that your client is capable of.



## Browser support

| Type   | Link                                                         |
| ------ | ------------------------------------------------------------ |
| Videos | https://en.wikipedia.org/wiki/HTML5_video#Browser_support    |
| Images | https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#supported_image_formats |
| Audios | https://en.wikipedia.org/wiki/HTML5_audio#Supported_audio_coding_formats |

**Note:** most browsers run Chromium. If your internet browser doesn't show up in the links above, check for the Chromium/Chrome compatibility list.