# Collections

You can think of collections as personal, virtual folders where you can group different type of files. Just like playlists are for audios, collections are for all files



## Creating a collection

Before you can start adding an file to a collection, you have to create one. Fortunately, you can create a collection right straight away from any file. Just open a file and click the "Collections" button. 

![Create a collection part 1](../assets/images/collections/create-1.png)

A window with a list containing all existing collections will appear. All you have to do is click the "New collection" button.

![Create a collection part 2](../assets/images/collections/create-2.png)

Give your collection a unique name and click the "Create" button. You will be taken back to the list containing all your collections, including the new one you just created.

![Create a collection part 3](../assets/images/collections/create-3.png)



## Adding a file to a collection

Just open the file you want to add to a collection and click the "Collections" button. 

![Click the Collection button](../assets/images/collections/create-1.png)

A window with a list containing all existing collections will appear. All you have to do is find the collection to which you want to add the file and click the "Add" button.

![Click the add button](../assets/images/collections/add.png)

If you see the "Remove" button, that means the file has been correctly added to the collection. You can now close the window by pressing the white X button in the top right corner of the window.



## Removing a file from a collection

Just open the file you want to remove from a collection and click the "Collections" button. 

![Click the Collection button](../assets/images/collections/create-1.png)

A window with a list containing all existing collections will appear. All you have to do is find the collection to which you want to remove the file from and click the "Remove" button.

![Click the remove button](../assets/images/collections/remove.png)

If you see the "Add" button, that means the file has been correctly removed from the collection. You can now close the window by pressing the white X button in the top right corner of the window.



## Where to find your collections

You can find your collections in your home page.

![Home page with collections](../assets/images/collections/home.png)



## Renaming a collection

From the home page, open the collection you want to rename and then click the "Rename" button in the top right corner.

![Rename a collection part 1](../assets/images/collections/rename-1.png)

Choose a new name for you collection and click the "Rename" button.

![Rename a collection part 2](../assets/images/collections/rename-2.png)



## Deleting a collection

From the home page, open the collection you want to rename and then click the "Delete" button in the top right corner.

![Delete a collection part 1](../assets/images/collections/delete-3.png)

If you are sure you want to delete the collection, click the "Delete" button. If you want to cancel the operation, click the "Cancel" button.

**Attention:** a deleted collection cannot be retrieved. Files contained in a collection are not deleted from your computer, just from the collection.

![Delete a collection part 2](../assets/images/collections/delete-4.png)