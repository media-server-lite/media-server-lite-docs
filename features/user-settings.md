# User settings

You can customize your profile name and profile picture or manage your password easily.



## Opening the Settings page

From anywhere in Media Server Lite, you can click your name in the top right corner of the header and select "Settings"

![Opening settings page](../assets/images/settings/open.png)

A page like this one will open.

![Settings main page](../assets/images/settings/settings.png)

## Name and profile picture

Click the "User account" button to show settings about your name and profile picture.

![User account panel](../assets/images/settings/user-account.png)

In this panel you can upload (or remove) your profile picture and change your profile name (the one that appears in the login page and in the header)

After you're done making changes, simply click the "Save" button.



## Password

Click the "Security & password" button to show settings for creating, updating or removing your account's password.

## Adding a password

![Security & password panel](../assets/images/settings/security-and-password-1.png)

Simply type the password you want to set for your account, then repeat it in the second field to make sure you did not type it wrong and then click the "Update password" button.

## Updating the password

![Security & password panel to update the password](../assets/images/settings/security-and-password-2.png)

You need to type the current password if you want to change it, then simply type a new password and repeat it, then click the "Update password" button.



### Removing the password

![Security & password panel to remove the password](../assets/images/settings/security-and-password-2.png)

Simply type the current password and then click the "Remove password" button.