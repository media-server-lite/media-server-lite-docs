# Security

Since Media Server Lite is self-hosted, meaning you are the one powering the application, the risk of data breach and other security issues are drastically reduced. 

That doesn't mean that security best practices don't apply here.



Here is a list of things to keep in mind when running Media Server Lite:

- The application is based on Apache and PHP which are always shipped with the latest versions or security patches. At the moment of writing, Media Server Lite is shipped with Apache 2.4 and PHP 7.4.
  That doesn't mean that you're bulletproof. Always make sure that you're running the latest version of Media Server Lite in order to get the latest patches.
- You should not expose Media Server Lite on the public internet, unless you're well aware of the risks that you will be facing. As long as you keep the installation in the local network, you should be just fine.
- We recommend running Media Server Lite as a Docker container, that way the application is isolated from the host operating system. Although this requires more hardware resources than running a standalone installation, using a Docker container is considered to be safer. Please, refer to the requirements for your operating system: Windows, macOS, Linux. TODO LINK