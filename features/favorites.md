# Favorites

Favorites are used to pin folders and files to the home page and are account private, meaning each user has its own favorites. 



## Adding an item to favorites

Each folder and each file has a button "Add to favorites" in the top-right corner of the page. By clicking that button, you mark the item as favorite. You will then be able to find it in the home page.

![Add to favorites](../assets/images/favorites/add.png)



## Removing an item from favorites

Each folder and each file has a button "Remove from favorites" in the top-right corner of the page. By clicking that button, you unmark the item as favorite.

![Remove from favorites](../assets/images/favorites/remove.png)



## Where to find favorites

You can find your favorite folders and files in your home page.

![Home page with favorites](../assets/images/favorites/home.png)