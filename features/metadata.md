# Metadata

Metadata are information that can add to a file, be it a video, an audio or image.



## Adding metadata to a file

Simply open a file and click the "Add metadata" button.

![Add metadata part 1](../assets/images/metadata/create-1.png)

A panel with all the info you can add will appear. (if you see less fields, click the "Show more..." button).

![Add metadata part 1](../assets/images/metadata/create-2.png)

You can fill any field you like and click the "Save" button in the bottom right corner of the panel. Note that the "title" field is required.

![Complete metadata](../assets/images/metadata/full.png)

After saving, this is how metadata are shown when every field is filled.

![Add metadata part 1 ><](../assets/images/metadata/icon.png)

You can see metadata when opening a file or directly from the folder view, by clicking the "i" button next to a file. (this button only appears if metadata are available for that file)



## Getting metadata from Wikipedia

Instead of filling every field yourself and finding a good poster to upload, you can grab info from Wikipedia and save them to Media Server Lite in an automatic fashion.



Simply open a file and click the "Add metadata" button and paste a full Wikipedia URL in the "title" field.

![Get from Wikipedia with URL ><](../assets/images/metadata/create-5.png)

Click the "Get from Wikipedia" button and let Media Server Lite do the magic!

Currently supported Wikipedia languages:
* English
* Italian






## Storage

All text info are saved locally in the Media Server Lite database.

The poster has different save mechanisms:

- If you uploaded a poster, it will be stored in the Media Server Lite filesystem at `public/images/posters`
- If you used the "Get from Wikipedia" feature, it will be loaded from Wikipedia servers and won't be stored locally in the filesystem. This can have downsides if your device does not have internet access or if Wikipedia goes down.