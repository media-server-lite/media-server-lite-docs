# Users

Media Server Lite can have multiple users, thus separating what each one can see and do.



## User selection

When accessing the application, the first thing you will see is the user selection page, unless there's just one user available. In that case, the application will login automatically using the only user available.

![Login page](../assets/images/users/login.png)

*An example of login screen, featuring three different users*



## Protections

User accounts can be protected by a PIN (a numeric password), as shown in the screenshot below.

![PIN requested when logging in <](../assets/images/users/pin-protected-account.png)

This is to prevent others from using your account. You can set you account password in the settings page, right after logging in.