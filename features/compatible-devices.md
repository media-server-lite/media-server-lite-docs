# Compatible devices

## Host computer (server)

Media Server Lite is compatible with computers running Windows, macOS and Linux.

### Windows

Windows users can choose between two different type of installations:

- **MSL Manager:** this is a traditional installation which is the lightest one. It requires a PC running Windows 10 with at least 30 MB of RAM ad 130 MB of storage. 1 GB of RAM is preferred.
- **Docker:** this requires having [Docker](https://www.docker.com/) installed on your computer. Although it is heavier than using the MSL Manager, it is the safest one. It requires a PC capable of running Docker (see [Docker requirements](https://docs.docker.com/docker-for-windows/install/#system-requirements)) with at least 30 MB of RAM and 510 MB of storage. 1 GB of RAM is preferred. Docker on Windows can run in Hyper-V mode or WSL2 mode, we recommend the latter (see [Docker Desktop WSL2 backend](https://docs.docker.com/docker-for-windows/wsl/) for more).



### macOS

macOS users have to rely on [Docker](https://www.docker.com/) in order to run Media Server Lite.

The requirements are:

- A macOS computer capable of running Docker (see [Docker requirements](https://docs.docker.com/docker-for-mac/install/#system-requirements))
- At least 30 MB of RAM. 1 GB of RAM is preferred.
- At least 510 MB of storage



Since Media Server Lite is "just" an application powered by Apache and PHP, you are free to setup your own environment without using Docker. You may even use Nginx or any web server that supports PHP.

**Warning:** Media Server Lite does not officially supports running the application outside of Docker.



### Linux

Linux users have to rely on [Docker](https://www.docker.com/) in order to run Media Server Lite.

The requirements are:

- A computer capable of running Docker (see [Docker requirements](https://docs.docker.com/engine/install/#server))
- At least 30 MB of RAM. 1 GB of RAM is preferred.
- At least 510 MB of storage



Since Media Server Lite is "just" an application powered by Apache and PHP, you are free to setup your own environment without using Docker. You may even use Nginx or any web server that supports PHP.

**Warning:** Media Server Lite does not officially supports running the application outside of Docker.



### Other operating systems

Any system that can run Docker can easily run Media Server Lite. Check your operating system for Docker support.



Since Media Server Lite is "just" an application powered by Apache and PHP, you are free to setup your own environment without using Docker. You may even use Nginx or any web server that supports PHP.

**Warning:** Media Server Lite does not officially supports running the application outside of Docker.



## Viewers (client)

Media Server Lite supports every device in the world that is capable of running a modern web browser.

Here's a list of devices we tested for you:

- Android smartphones and tables
- iOS and iPadOS
- Windows, macOS and Linux computers
- Amazon Fire TV Stick (with Silk Browser)
- Samsung TVs (with Samsung Internet)