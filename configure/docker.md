# Windows, macOS, Linux (Docker)

![Docker Logo <](../assets/images/logo-docker-small.png)

This documentation will guide you through configuring Media Server Lite installed via Docker.



## Prerequisites

- Having [installed Media Server Lite via Docker](/iuu/docker)
- (Linux only): administrator privileges, [an user belonging to the Docker group](https://docs.docker.com/engine/install/linux-postinstall/) or [rootless mode](https://docs.docker.com/engine/security/rootless/)



## Start

Simply run the following command in your Terminal:

```bash
docker start [CONTAINER_NAME]
```



## Stop

Simply run the following command in your Terminal:

```bash
docker stop [CONTAINER_NAME]
```



## Restart

Simply run the following command in your Terminal:

```bash
docker restart [CONTAINER_NAME]
```



## Uninstall

First, stop the container if it's running, then simply run the following command in your Terminal:

```bash
docker rm [CONTAINER_NAME]
```



## Docker Compose

Although the [installation guide](/iuu/docker) shows how to start using Media Server Lite with a docker run command, it's recommended to use docker-compose.



### Why using docker-compose instead of the docker run command?

Docker Compose empowers you with a better management of the lifecycle of the container and configuration of the application, by allowing you to change configurations, map volumes and more in an easier way compared to a simple docker run command.



### Template

```docker-compose.yml
version: "3.8"

services:
  media-server-lite:
    image: "fsorge/msl"
    restart: "unless-stopped"
    ports:
      - 616:80
    volumes:
      MAP_YOUR_FOLDERS_HERE
    tty: true
```

**media-server-lite** will be the name of the service. You are free to customize it. Keep it in mind since we'll use it further in the guide.



### Folders sharing

In the "volumes" section you can decide which folders Media Server Lite should be able to see (and show in the home page for every user).

You can map folders like this:

```docker-compose.yml
 - ./family-pictures:/var/www/html/public/media/family-pictures
```

The important part is that:

- There's just one folder per row
- All lines starts with a whitespace, a dash, a whitespace
- You can have relative paths (eg: `./family-pictures`) or absolute paths (eg: `/home/user/family-pictures`) on the left side
- On the right side, it must always start with `/var/www/html/public/media/` (which is where Media Server Lite reads shared folders) and end with the name of the folder you are sharing



### Database

We recommend also mapping the database to an external folder to make sure that it doesn't get erased on container removal or update.

1. In order to map the database, run the following command (when the container is running):

```bash
docker cp [CONTAINER_NAME]:/var/www/html/database/sqlite/database.sqlite ./database.sqlite
```

This will copy the database from the container out to your computer in the current directory (where your terminal is open).

2. You then have to map the database to the one we just exported. In order to do that, add a row in the "volumes" section of your docker-compose.yml file:

```docker-compose.yml
 - ./database.sqlite:/var/www/html/database/sqlite/database.sqlite
```

&nbsp;

**One more thing:** having the database exported allows you to access the database directly (for example via DB Browser (SQLite)).



### Avatars

We also recommend mapping the avatars folder to an external one to make sure that it doesn't get erased on container removal or update.

You have to map the avatars folder to one in your computer. In order to do that, add a row in the "volumes" section of your docker-compose.yml file:

```docker-compose.yml
 - ./avatars:/var/www/html/public/images/avatars
```



### Posters

We also recommend mapping the posters folder to an external one to make sure that it doesn't get erased on container removal or update.

You have to map the posters folder to one in your computer. In order to do that, add a row in the "volumes" section of your docker-compose.yml file:

```docker-compose.yml
 - ./posters:/var/www/html/public/images/posters
```



### Starting MSL with Docker Compose

When you're done configuring your docker-compose.yml file, open a Terminal and write:

```bash
docker-compose up -d
```

This will start the application with your configurations. You can also use this command to restart the application in case you change the `docker-compose.yml` file with new settings and mappings.



## CLI commands

**Note:** if you used Docker run instead of Docker Compose, change `docker-compose` with `docker run` and `SERVICE_NAME` with `CONTAINER_NAME`.

### Create new user

```bash
docker-compose exec [SERVICE_NAME] php artisan user:create
```

It will guide you through the steps of creating a non-admin user. You will be able to choose a name, a username and a password for the new user.



### DistUpgrade

```bash
docker-compose exec [SERVICE_NAME] php artisan distupgrade
```

This command updates your database with latest group policies patches. It is automatically executed on every launch and update of the application.