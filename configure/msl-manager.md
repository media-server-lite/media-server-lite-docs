# Media Server Lite Manager

![MSL Manager Logo <](../assets/images/logo-manager-small.png)

This documentation will guide you through the post-installation steps and configurations steps to get your Media Server Lite up and running.



## Prerequisites

- The latest version of [MSL Manager](https://msl.francescosorge.com/#/download/windows)
- Having [installed Media Server Lite via MSL Manager](/iuu/msl-manager)



## Launch

Double click the "Media Server Lite Manager.exe" file and you will be greeted by this simple welcome screen.

![Main window ><](../assets/images/manager/main.png)

Click the "Manage" button.

It will ask you where Media Server Lite is. Browse your computer until you find where you installed Media Server Lite, select its folder and confirm.



## The user interface

![Main manage window ><](../assets/images/manager/manage.png)

The window is made of:

- **A header:** containing the application title and a Start/Stop button
- **Two tabs:**
  - **Main:**
    - **Webapp URL:** which is the URL you have to navigate to in order to use the application and view its streams
    - **Main directory:** where Media Server Lite is installed
    - **Data directory:** where Media Server Lite stores personal data that are not removed when updating the application and can be kept when uninstalling the application
  - **Media:**
    - **List:** this list contains all shared files and folders with Media Server Lite
    - **Open media:**  opens the media folder where Media Server Lite creates symbolic links and junctions with your files and folders
    - **Remove:** removes an entry from the shared list, thus preventing Media Server Lite to access it
    - **Add folder:** adds a folder for sharing that will appear on every users' home page



## Start & stop

In order to start (or stop) Media Server Lite, use the dedicated button "Start" (or "Stop") available at the top of the window.

Once your application starts, you can click the "Open" button at the right of the "Webapp URL" label. This will open your default browser and navigate to the application URL.

**Note:** starting and stopping the application requires administrator privileges.



## Main tab

In the main tab you can see and open the main application's folders and URL.

It's just an informative tab and it's not that important.



## Media tab

![Media manage window ><](../assets/images/manager/manage-2.png)

In this tab you can manage which folders you want to share with Media Server Lite.

Media Server Lite can only see the folders you add in this view and nothing more, shielding your personal data from potential attacks.



### Share a folder

1. Navigate to the "Media" tab
2. Click the "Add folder" button
3. Browse your computer to the folder you want to share
4. Confirm the selection
5. The folder should now be correctly shared



### Un-share a folder

1. Navigate to the "Media" tab
2. Select the folder you want to remove
3. Click the "Remove" button
4. The folder should now be correctly unshared

**Note:** removing a folder does not delete the folder nor its contents. It simply unlinks the folder with Media Server Lite.