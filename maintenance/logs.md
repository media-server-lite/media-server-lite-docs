# Logs

Media Server Lite writes errors and other info to a file located in `storage/logs/laravel.log`.



## MSL Manager

### Open

If you installed Media Server Lite with MSL Manager, you can simply open the file with a text editor.

Should you not find the file, it means there are no logs available.



### Clear

In order to clear the logs, simply delete the file.



## Docker

### Open

If you installed Media Server Lite with Docker Compose, open a Terminal and write:

```bash
docker-compose exec [SERVICE_NAME] cat /var/www/html/storage/logs/laravel.log
```

If you used Docker Run to bring up the application, write:

```bash
docker exec [CONTAINER_NAME] cat /var/www/html/storage/logs/laravel.log
```



Should the error "No such file or directory" come up, it means there are no logs available.



### Clear

In order to clear the logs, simply delete the file.

If you installed Media Server Lite with Docker Compose, open a Terminal and write:

```bash
docker-compose exec [SERVICE_NAME] rm /var/www/html/storage/logs/laravel.log
```

If you used Docker Run to bring up the application, write:

```bash
docker exec [CONTAINER_NAME] rm /var/www/html/storage/logs/laravel.log
```

