# Media Server Lite

> 📺 MSL is a local, privacy-focused, web application that allows you to share folders with devices such as TVs, smartphones and many more

This is the official documentation of Media Server Lite. If you are looking for the main website, you should click [this link](https://msl.francescosorge.com).



## What you will find

You will find basic guides about installing Media Server Lite, configuring it, troubleshooting and the like and you will also find advanced articles about how Media Server Lite works, how to tweak it in details, how to contribute or build your own version of Media Server Lite and so on.



## For whom this documentation is

This documentation is meant to be both for normal people and technological enthusiasts.



## Where to start

Use the left sidebar to search what you are looking for.
