* [**Home**](/)
* [**Changelogs**](changelogs.md)
* **Features**
  * [Users](features/users.md)
  * [Security](features/security.md)
  * [Favorites](features/favorites.md)
  * [History](features/history.md)
  * [Collections](features/collections.md)
  * [Resume](features/resume.md)
  * [Metadata](features/metadata.md)
  * [User settings](features/user-settings.md)
  * [Compatible devices](features/compatible-devices.md)
  * [Compatible files](features/compatible-files.md)
* **Install, Update, Uninstall**
  * [MSL Manager (Windows)](iuu/msl-manager.md)
  * [Windows, macOS, Linux](iuu/docker.md)
  * [Another operating system](iuu/another-os.md)
* **Configure**
  * [MSL Manager (Windows)](configure/msl-manager.md)
  * [Docker](configure/docker.md)
* **Maintenance**
  * [Logs](maintenance/logs.md)
  * <!--[Health](maintenance/health.md)-->
* <!--[**Errors**](errors/)-->
* <!--[**Contributing**](contributing/)-->

